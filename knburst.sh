#!/bin/bash

KSVC_URL="$(kubectl get ksvc python-app -o jsonpath='{.status.url}')"

siege -r 4 -c 255 -d 1 -v "$KSVC_URL"
